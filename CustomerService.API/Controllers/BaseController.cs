using System.Collections.Generic;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace CustomerService.API.Controllers
{
    public class BaseController : ControllerBase
    {
        public BadRequestObjectResult BadRequest(List<ValidationFailure> errors)
        {
            var messages = new List<string>();

            errors.ForEach(error => messages.Add(error.ErrorMessage));

            return base.BadRequest(messages);
        }
    }
}