using System;
using System.Threading.Tasks;
using CustomerService.Application.Features.Customers.Commands.RegisterNewCustomer;
using CustomerService.Application.Features.Customers.Commands.UpdateCustomerInfo;
using CustomerService.Application.Features.Customers.Queries.GetCustomerById;
using CustomerService.Application.Features.Customers.Queries.GetCustomerDetails;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CustomerService.API.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomersController : BaseController
    {
        private readonly IMediator _mediator;

        public CustomersController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet("{id:guid}", Name = "GetCustomerDetails")]
        public async Task<IActionResult> GetDetails([FromRoute] GetCustomerDetails getCustomerDetails)
        {
            var customer = await _mediator.Send(getCustomerDetails);
            return customer == null
                ? NotFound($"Customer with id {getCustomerDetails.Id} cannot be found.")
                : Ok(customer);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterNewCustomer registerNewCustomer,
            [FromQuery] string type)
        {
            registerNewCustomer.CustomerType = type;
            var (errors, customer) = await _mediator.Send(registerNewCustomer);
            if (errors != null) return BadRequest(errors);
            return CreatedAtRoute("GetCustomerDetails", new {id = customer.Id}, customer);
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] UpdateCustomerInfo updateCustomerInfo,
            [FromRoute] GetCustomerById getCustomerById)
        {
            var customer = await _mediator.Send(getCustomerById);
            if (customer == null) return NotFound($"Customer with id {getCustomerById.Id} cannot be found!");

            updateCustomerInfo.Customer = customer;
            var (errors, updatedCustomer) = await _mediator.Send(updateCustomerInfo);
            if (errors != null) return BadRequest(errors);
            return Ok(updatedCustomer);
        }
    }
}