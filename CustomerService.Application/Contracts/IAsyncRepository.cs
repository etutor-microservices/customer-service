using System.Threading.Tasks;

namespace CustomerService.Application.Contracts
{
    public interface IAsyncRepository<T>
    {
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
    }
}