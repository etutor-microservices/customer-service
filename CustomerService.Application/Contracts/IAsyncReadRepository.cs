﻿using System;
using System.Threading.Tasks;
using CustomerService.Domain.Common;

namespace CustomerService.Application.Contracts
{
    public interface IAsyncReadRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetByIdAsync(Guid id);
    }
}