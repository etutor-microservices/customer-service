using System.Threading.Tasks;
using CustomerService.Domain.CustomerAggregateRoot;

namespace CustomerService.Application.Contracts
{
    public interface ICustomersRepository : IAsyncReadRepository<Customer>, IAsyncRepository<Customer>
    {
        Task<Customer> GetByEmail(string email);
        Task<Customer> GetByPhoneNumber(string phoneNumber);
    }
}