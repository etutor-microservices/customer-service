using System;
using CustomerService.Domain.CustomerAggregateRoot;

namespace CustomerService.Application.Features.Customers.ViewModels
{
    public class CustomerDetailsVm
    {
        public Customer Customer { get; set; }
        public Guid Id { get; init; }
        public string FirstName { get; init; }
        public string MiddleName { get; init; }
        public string LastName { get; init; }
        public DateTime DateOfBirth { get; init; }
        public string PhoneNumber { get; init; }
        public string Gender { get; init; }
        public Guid? SupervisorId { get; init; }
        public string Email { get; init; }
    }
}