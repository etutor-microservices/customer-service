using System;
using System.Threading;
using System.Threading.Tasks;
using CustomerService.Application.Contracts;
using CustomerService.Domain.CustomerAggregateRoot;
using MediatR;

namespace CustomerService.Application.Features.Customers.Queries.GetCustomerById
{
    public class GetCustomerByIdHandler : IRequestHandler<GetCustomerById, Customer>
    {
        private readonly ICustomersRepository _customersRepository;

        public GetCustomerByIdHandler(ICustomersRepository customersRepository)
        {
            _customersRepository = customersRepository ?? throw new ArgumentNullException(nameof(customersRepository));
        }

        public async Task<Customer> Handle(GetCustomerById request, CancellationToken cancellationToken)
        {
            return await _customersRepository.GetByIdAsync(request.Id);
        }
    }
}