using System;
using CustomerService.Domain.CustomerAggregateRoot;
using MediatR;

namespace CustomerService.Application.Features.Customers.Queries.GetCustomerById
{
    public class GetCustomerById : IRequest<Customer>
    {
        public Guid Id { get; init; }
    }
}