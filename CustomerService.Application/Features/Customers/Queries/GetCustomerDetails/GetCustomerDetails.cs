using System;
using CustomerService.Application.Features.Customers.ViewModels;
using MediatR;

namespace CustomerService.Application.Features.Customers.Queries.GetCustomerDetails
{
    public class GetCustomerDetails : IRequest<CustomerDetailsVm>
    {
        public Guid Id { get; init; }
    }
}