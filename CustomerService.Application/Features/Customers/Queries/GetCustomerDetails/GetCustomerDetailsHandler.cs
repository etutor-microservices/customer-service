using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CustomerService.Application.Contracts;
using CustomerService.Application.Features.Customers.ViewModels;
using MediatR;

namespace CustomerService.Application.Features.Customers.Queries.GetCustomerDetails
{
    public class GetCustomerDetailsHandler : IRequestHandler<GetCustomerDetails, CustomerDetailsVm>
    {
        private readonly ICustomersRepository _customersRepository;
        private readonly IMapper _mapper;

        public GetCustomerDetailsHandler(ICustomersRepository customersRepository, IMapper mapper)
        {
            _customersRepository = customersRepository ?? throw new ArgumentNullException(nameof(customersRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<CustomerDetailsVm> Handle(GetCustomerDetails request, CancellationToken cancellationToken)
        {
            var customer = await _customersRepository.GetByIdAsync(request.Id);
            return customer == null ? null : _mapper.Map<CustomerDetailsVm>(customer);
        }
    }
}