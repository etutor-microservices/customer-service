using System;
using CustomerService.Application.Contracts;
using CustomerService.Domain.Enums;
using FluentValidation;

namespace CustomerService.Application.Features.Customers.Commands.RegisterNewCustomer
{
    public class RegisterNewCustomerValidator : AbstractValidator<RegisterNewCustomer>
    {
        public RegisterNewCustomerValidator(ICustomersRepository customersRepository)
        {
            RuleFor(gnc => gnc.FirstName).MaximumLength(50).NotEmpty().NotNull();
            RuleFor(gnc => gnc.MiddleName).MaximumLength(50).NotEmpty().NotNull();
            RuleFor(gnc => gnc.LastName).MaximumLength(50).NotEmpty().NotNull();

            RuleFor(gnc => gnc.PhoneNumber).MaximumLength(20).NotEmpty().NotNull()
                .MustAsync(async (phoneNumber, _) =>
                {
                    var customer = await customersRepository.GetByPhoneNumber(phoneNumber);
                    return customer == null;
                }).WithMessage("Phone number has already exist!");

            RuleFor(gnc => gnc.Email).EmailAddress().NotEmpty().NotNull()
                .MustAsync(async (email, _) =>
                {
                    var customer = await customersRepository.GetByEmail(email);
                    return customer == null;
                }).WithMessage("Email has already exist!");

            RuleFor(gnc => gnc.CustomerType).NotEmpty().NotNull()
                .Must(type => Enum.TryParse<CustomerType>(type, true, out _))
                .WithMessage("Customer type is not accepted. Only 'student', 'supervisor' are accepted.");

            RuleFor(gnc => gnc.Gender).NotEmpty().NotNull().Must(type => Enum.TryParse<Gender>(type, true, out _))
                .WithMessage("Gender is not accepted. Only 'male', 'female' are accepted.");
        }
    }
}