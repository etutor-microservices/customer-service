using System;
using System.Collections.Generic;
using CustomerService.Application.Features.Customers.ViewModels;
using FluentValidation.Results;
using MediatR;

namespace CustomerService.Application.Features.Customers.Commands.RegisterNewCustomer
{
    public class RegisterNewCustomer : IRequest<(List<ValidationFailure> errors, CustomerDetailsVm customer)>
    {
        public string FirstName { get; init; }
        public string MiddleName { get; init; }
        public string LastName { get; init; }
        public string Email { get; init; }
        public DateTime DateOfBirth { get; init; }
        public string Gender { get; init; }
        public string PhoneNumber { get; init; }
        public string CustomerType { get; set; }
        public Guid? SupervisorId { get; set; }
    }
}