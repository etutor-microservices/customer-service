using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CustomerService.Application.Contracts;
using CustomerService.Application.Features.Customers.ViewModels;
using CustomerService.Domain.CustomerAggregateRoot;
using CustomerService.Domain.Enums;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace CustomerService.Application.Features.Customers.Commands.RegisterNewCustomer
{
    public class RegisterNewCustomerHandler : IRequestHandler<RegisterNewCustomer, (List<ValidationFailure> errors,
        CustomerDetailsVm customer)>
    {
        private readonly ICustomersRepository _customersRepository;
        private readonly IValidator<RegisterNewCustomer> _validator;
        private readonly IMapper _mapper;

        public RegisterNewCustomerHandler(ICustomersRepository customersRepository,
            IValidator<RegisterNewCustomer> validator, IMapper mapper)
        {
            _customersRepository = customersRepository ?? throw new ArgumentNullException(nameof(customersRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, CustomerDetailsVm customer)> Handle(
            RegisterNewCustomer request, CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            var customer = InstantiateCustomer(request);
            var insertedCustomer = await _customersRepository.AddAsync(customer);

            return (null, _mapper.Map<CustomerDetailsVm>(insertedCustomer));
        }

        private static Customer InstantiateCustomer(RegisterNewCustomer request)
        {
            var customer = Enum.Parse<CustomerType>(request.CustomerType, true) switch
            {
                CustomerType.Student => new Student(request.FirstName, request.MiddleName, request.LastName,
                    request.Email, request.DateOfBirth, Enum.Parse<Gender>(request.Gender, true), request.PhoneNumber,
                    request.SupervisorId),

                CustomerType.Supervisor => new Supervisor(request.FirstName, request.MiddleName, request.LastName,
                    request.Email, request.DateOfBirth, Enum.Parse<Gender>(request.Gender, true), request.PhoneNumber),

                _ => new Customer(request.FirstName, request.MiddleName, request.LastName, request.Email,
                    request.DateOfBirth, Enum.Parse<Gender>(request.Gender, true), request.PhoneNumber)
            };

            return customer;
        }
    }
}