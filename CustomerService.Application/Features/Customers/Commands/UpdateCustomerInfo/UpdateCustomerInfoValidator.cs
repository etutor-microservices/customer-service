using System;
using CustomerService.Domain.Enums;
using FluentValidation;

namespace CustomerService.Application.Features.Customers.Commands.UpdateCustomerInfo
{
    public class UpdateCustomerInfoValidator : AbstractValidator<UpdateCustomerInfo>
    {
        public UpdateCustomerInfoValidator()
        {
            RuleFor(uci => uci.FirstName).MaximumLength(50).NotEmpty().NotNull()
                .When(uci => !string.IsNullOrEmpty(uci.FirstName));

            RuleFor(uci => uci.MiddleName).MaximumLength(50).NotEmpty().NotNull()
                .When(uci => !string.IsNullOrEmpty(uci.MiddleName));

            RuleFor(uci => uci.LastName).MaximumLength(50).NotEmpty().NotNull()
                .When(uci => !string.IsNullOrEmpty(uci.LastName));

            RuleFor(uci => uci.Email).EmailAddress().NotEmpty().NotNull()
                .When(uci => !string.IsNullOrEmpty(uci.Email));

            RuleFor(uci => uci.PhoneNumber).MaximumLength(20).NotEmpty().NotNull()
                .When(uci => !string.IsNullOrEmpty(uci.PhoneNumber));

            RuleFor(gnc => gnc.CustomerType).NotEmpty().NotNull()
                .Must(type => Enum.TryParse<CustomerType>(type, true, out _))
                .WithMessage("Customer type is not accepted. Only 'student', 'supervisor' are accepted.")
                .When(gnc => gnc.CustomerType != null);

            RuleFor(gnc => gnc.Gender).NotEmpty().NotNull().Must(type => Enum.TryParse<Gender>(type, true, out _))
                .WithMessage("Gender is not accepted. Only 'male', 'female' are accepted.")
                .When(gnc => gnc.Gender != null);
        }
    }
}