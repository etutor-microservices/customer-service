using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CustomerService.Application.Contracts;
using CustomerService.Application.Features.Customers.ViewModels;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace CustomerService.Application.Features.Customers.Commands.UpdateCustomerInfo
{
    public class UpdateCustomerInfoHandler : IRequestHandler<UpdateCustomerInfo, (List<ValidationFailure> errors,
        CustomerDetailsVm customer)>
    {
        private readonly ICustomersRepository _customersRepository;
        private readonly IValidator<UpdateCustomerInfo> _validator;
        private readonly IMapper _mapper;

        public UpdateCustomerInfoHandler(ICustomersRepository customersRepository,
            IValidator<UpdateCustomerInfo> validator, IMapper mapper)
        {
            _customersRepository = customersRepository ?? throw new ArgumentNullException(nameof(customersRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, CustomerDetailsVm customer)> Handle(
            UpdateCustomerInfo request, CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            request.Customer.UpdateFirstName(request.FirstName);

            await _customersRepository.UpdateAsync(request.Customer);

            return (null, _mapper.Map<CustomerDetailsVm>(request.Customer));
        }
    }
}