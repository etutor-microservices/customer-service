using AutoMapper;
using CustomerService.Application.Features.Customers.ViewModels;
using CustomerService.Domain.CustomerAggregateRoot;

namespace CustomerService.Application.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerDetailsVm>();
        }
    }
}