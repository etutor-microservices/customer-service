import pika
from core.config import settings

class MetaClass(type):

    _instance ={}

    def __call__(cls, *args, **kwargs):

        """ Singleton """

        if cls not in cls._instance:
            cls._instance[cls] = super(MetaClass, cls).__call__(*args, **kwargs)
            return cls._instance[cls]


class RabbitMQ(metaclass=MetaClass):

    def __init__(self, queue='hello', routingKey='hello', exchange=''):
        """ Configure RabbitMQ Server  """
        self.queue = queue
        self._url = pika.URLParameters(settings.CELERY_BROKER_URL)
        self.routingKey = routingKey
        self.exchange = exchange


class Producer():

    def __init__(self, server):

        """
        server: Object of RabbitMQ
        """

        self.server = server
        self._connection = pika.BlockingConnection(self.server._url)
        self._channel = self._connection.channel()
        self._channel.queue_declare(queue=self.server.queue)

    def publish(self, payload: dict = {}):

        """
        :param payload: JSON payload
        :return: None
        """

        self._channel.basic_publish(exchange=self.server.exchange,
                      routing_key=self.server.routingKey,
                      body=str(payload))

        print("Published Message: {}".format(payload))
        self._connection.close()


if __name__ == "__main__":
    server = RabbitMQ(queue='hello', routingKey='hello', exchange='')

    rabbitmq = Producer(server)
    rabbitmq.publish(payload={"Data":22})




