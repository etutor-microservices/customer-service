from typing import Any

async def form2buffer(form_images) -> Any:
    protos = []
    for f in form_images:
        buffer = await f.read()
        protos.append({
                        'filename' :f.filename,
                        'buffer': buffer,
                        'mimetype': f.content_type.split("/")[-1] })
    
    return protos