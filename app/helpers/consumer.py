import pika
from core.config import settings

class MetaClass(type):

    _instance ={}

    def __call__(cls, *args, **kwargs):

        """ Singelton """

        if cls not in cls._instance:
            cls._instance[cls] = super(MetaClass, cls).__call__(*args, **kwargs)
            return cls._instance[cls]


class RabbitMQ(metaclass=MetaClass):

    def __init__(self, queue='hello'):

        """ Server initialization   """

        self._url = pika.URLParameters(settings.CELERY_BROKER_URL)
        self.queue = queue


class Consumer():

    def __init__(self, server):

        """

        :param server: Object of class RabbitMqServerConfigure
        """

        self.server = server
        self._connection = pika.BlockingConnection(self.server._url)
        self._channel = self._connection.channel()
        self._tem = self._channel.queue_declare(queue=self.server.queue)
        print("Server started waiting for Messages ")

    @staticmethod
    def callback(ch,method, properties, body):

        Payload = body.decode("utf-8")
        print(type(Payload))
        print("Data Received : {}".format(Payload))



    def startserver(self):
        self._channel.basic_consume(
            queue=self.server.queue,
            on_message_callback=Consumer.callback(),
            auto_ack=True)
        self._channel.start_consuming()


if __name__ == "__main__":
    serverconfigure = RabbitMQ(queue='hello')

    server = Consumer(server=serverconfigure)
    server.startserver()
