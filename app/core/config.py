import os
from pydantic import BaseSettings

BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../'))

class CommonSettings(BaseSettings):
    PROJECT_NAME: str = os.environ.get("APP_NAME", "eTutor")
    SECRET_KEY: str = os.environ.get("SECRET", "2839fhd2i9dh2i3d289ncdow90c283dfhsiu13dbfskdjb")
    # MAIL_USERNAME: str = os.environ.get("MAIL_USERNAME", "orenbeworldwide@gmail.com")
    # MAIL_PASSWORD: str = os.environ.get("MAIL_PASSWORD", 'extbrsvhzwguzmms')
    # ACCESS_TOKEN_EXPIRE_MINUTES: int = os.environ.get("ACCESS_TOKEN_EXPIRE_MINUTES", 15)
    API_PREFIX = ''


class ServerSettings(BaseSettings):
    HOST: str = os.environ.get("HOST", "localhost")
    PORT: int = os.environ.get("PORT", 8000)
    EXTERNAL_HOST: str = os.environ.get("EXTERNAL_HOST", "localhost")
    EXTERNAL_PORT: int = os.environ.get("EXTERNAL_PORT", 3000)
    BACKEND_CORS_ORIGINS: str = os.environ.get("BACKEND_CORS_ORIGINS", "*")
    LOGGING_CONFIG_FILE: str = os.path.join(BASE_DIR, 'logging.ini')
    CELERY_BROKER_URL: str = os.environ.get("CELERY_BROKER_URL", 'amqps://yrydsnuu:nBD_oxngzMN8_SJGTlnhYnNet8WJY4Be@cougar.rmq.cloudamqp.com/yrydsnuu')
    CELERY_RESULT_BACKEND: str = os.environ.get("CELERY_RESULT_BACKEND", 'redis://localhost:6379')
    GRPC_CONNECTION: str = os.environ.get("GRPC_CONNECTION", "localhost:3099")
    RABBIT_MQ_URI:str = os.environ.get('RABBIT_MQ_URI', 'cougar.rmq.cloudamqp.com/yrydsnuu')
    RABBIT_MQ_USER:str = os.environ.get('RABBIT_MQ_USER', 'yrydsnuu')
    RABBIT_MQ_PASSWORD:str = os.environ.get('RABBIT_MQ_PASSWORD', 'nBD_oxngzMN8_SJGTlnhYnNet8WJY4Be')
    RABBIT_VIRTUAL_HOST:str = os.environ.get('RABBIT_VIRTUAL_HOST', '')


class DatabaseSettings(BaseSettings):
    DB_MONGO_URI: str = os.environ.get("DB_MONGO_URI", 'mongodb+srv://tinysm:220641@cluster0.qt8ln.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
    DB_MONGO_NAME: str = os.environ.get("DB_MONGO_NAME", 'TINYSM')


class Settings(CommonSettings, ServerSettings, DatabaseSettings):
    pass


settings = Settings()