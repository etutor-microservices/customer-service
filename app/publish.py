# publish.py
import pika
import json
import uuid
from core.config import settings

params = pika.URLParameters(settings.CELERY_BROKER_URL)
connection = pika.BlockingConnection(params)
channel = connection.channel()
channel.exchange_declare(
    exchange='order',
    exchange_type='direct'
)
order = {
    'id': str(uuid.uuid4()),
    'user_email': 'john.doe@example.com',
    'product': 'Jacket',
    'quantity': 1
}
channel.basic_publish(
    exchange='order',
    routing_key='order.notify',
    body=json.dumps(order)
)
print(' [x] Sent notify message')

connection.close()

