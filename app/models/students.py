from typing import Optional
from pydantic import BaseModel, Field

class Student(BaseModel):
    student_id: str = Field(...) # customer_id masked by the name student_id
    supervisor_id: str = Field(...)
    class Config:
        schema_extra = {
            "example": {
                "student_id": "tin",
                "supervisor_id": "trung",
            }
        }

class AlterStudentSchema(BaseModel):
    student_id: Optional[str]
    supervisor_id: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "student_id": "tin",
                "supervisor_id": "trung",
            }
        }
