from typing import Optional
from pydantic import BaseModel, Field

class Supervisor(BaseModel):
    supervisor_id: str = Field(...) # customer_id masked by the name supervisor_id
    class Config:
        schema_extra = {
            "example": {
                "supervisor_id": "tin",
            }
        }

class AlterSupervisorSchema(BaseModel):
    supervisor_id: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "supervisor_id": "tin",
            }
        }
