from typing import List, Optional
from enum import Enum
import uuid
from fastapi.datastructures import UploadFile
from fastapi.param_functions import File, Form
from pydantic import BaseModel, EmailStr, Field
from datetime import datetime

class Gender(str, Enum):
    Male = 'Male'
    Female = 'Female'
    Other = 'Other'
    NotGiven = 'NotGiven'

class Customer(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    firstname: str = Field(...)
    middlename: str = Field(default='')
    lastname: str = Field(default='')
    dob: datetime = Field(...)
    email: EmailStr = Field(...)
    description: str = Field(default='')
    gender: Gender = Field(default=Gender.NotGiven.value)
    phone: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "firstname": "tin",
                "middlename": "trung",
                "lastname": "nguyen",
                "dob": datetime.utcnow(),
                "email": "tin@gmail.com",
                "description": "",
                "gender": "Male",
                "phone": "0123456789",
            }
        }

class TargetCustomer(str, Enum):
    Supervisor = 'Supervisor'
    Student = 'Student'

class ProtoUploadFile(BaseModel):
    filename: str = Field(...)
    buffer: bytes = File(...)
    mimetype: str = Field(...)

class InboundCustomer(BaseModel):
    firstname: str 
    dob: datetime 
    email: EmailStr 
    gender: Gender 
    phone: str 
    target: TargetCustomer 
    supervisor_id: str
    images: List[UploadFile]

    @classmethod
    def as_form(
        cls,
        firstname: str = Form(...),
        dob: str = Form(...),
        email: str = Form(...),
        gender: str = Form(...),
        phone: str = Form(...),
        target: str = Form(...),
        supervisor_id: str = Form(...),
        images: List[UploadFile] = File(...)
    ):
        return cls(
            firstname=firstname,
            dob=dob,
            email=email,
            gender=gender,
            phone=phone,
            target=target,
            supervisor_id=supervisor_id,
            images=images
        )

class AlterCustomerSchema(BaseModel):
    firstname: Optional[str]
    middlename: Optional[str]
    lastname: Optional[str]
    dob: Optional[datetime]
    email: Optional[EmailStr]
    description: Optional[str]
    gender: Optional[Gender]
    phone: Optional[str]
    target: Optional[TargetCustomer]

    class Config:
        schema_extra = {
            "example": {
                "firstname": "tin",
                "middlename": "trung",
                "lastname": "nguyen",
                "dob": datetime.utcnow(),
                "email": "tin@gmail.com",
                "description": "",
                "gender": "Male",
                "phone": "0123456789",
                "target": "Customer",
            }
        }


class AwesomeForm(BaseModel):
    username: str
    password: str
    file: UploadFile

    @classmethod
    def as_form(
        cls,
        username: str = Form(...),
        password: str = Form(...),
        file: UploadFile = File(...)
    ):
        return cls(
            username=username,
            password=password,
            file=file
        )