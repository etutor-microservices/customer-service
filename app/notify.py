# notify.py
import pika
import json
from core.config import settings


params = pika.URLParameters(settings.CELERY_BROKER_URL)
connection = pika.BlockingConnection(params)
channel = connection.channel()

queue = channel.queue_declare('order_notify')
# queue_name = str(queue.method.queue)

# channel.queue_bind(
#     exchange='order',
#     queue=queue_name,
#     routing_key='order.notify'  # binding key
# )


def callback(ch, method, properties, body):
    payload = json.loads(body)
    print(' [x] Notifying {}'.format(payload))
    print(' [x] Done')
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_consume(queue='order_notify', on_message_callback=callback)

print(' [*] Waiting for notify messages. To exit press CTRL+C')

channel.start_consuming()
