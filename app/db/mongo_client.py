from motor.motor_asyncio import AsyncIOMotorClient
from core.config import settings

_client = AsyncIOMotorClient(settings.DB_MONGO_URI)

_database = _client[settings.DB_MONGO_NAME]

def get_db(collection: str):
    return _database.get_collection(collection)