from fastapi import APIRouter
from starlette.responses import HTMLResponse
from api import api_healthcheck, customer
from core.config import settings

router = APIRouter()

@router.get("/", tags=["Root"], response_class=HTMLResponse)
async def root():
    return """
    <html>
        <head>
            <title>eTutor Customer Service | FastAPI</title>
        </head>
        <body>
            <h1 style="text-align: center">Gotcha! Browse to <a href="/docs">/docs</a> for details!</h1>
        </body>
    </html>
    """
router.include_router(api_healthcheck.router, tags=["HealthCheck"], prefix="/healthcheck")
router.include_router(customer.router, tags=["Customers"], prefix="/customers")
# router.include_router(api_login.router, tags=["login"], prefix="/login")
# router.include_router(api_register.router, tags=["register"], prefix="/register")
# router.include_router(api_user.router, tags=["user"], prefix="/users")
