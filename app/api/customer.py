from typing import List, Optional
from fastapi import APIRouter, Body, HTTPException, Request
from fastapi.encoders import jsonable_encoder
from fastapi.param_functions import Depends
from starlette.responses import JSONResponse
from helpers.buffergrpc import form2buffer
from services.student import StudentService
from services.supervisor import SupervisorService
from helpers.consumer import RabbitMQ, Consumer
from models.customers import Customer, AlterCustomerSchema, InboundCustomer, TargetCustomer, ProtoUploadFile
from services.customer import CustomerService, RequestAvatarCustomerCreation


router = APIRouter()

@router.get("/{id}", response_description="Receive customer data with provided ID.")
async def get_customer_by_id(id: Optional[str] = None):
    customer = await CustomerService().get_customer_by_id(id=id)
    if customer is not None:
        return JSONResponse(customer)
    raise HTTPException(status_code=404, detail=f"Customer ID: {id} not found")


@router.get("/", response_description="Receive customer data .")
async def get_customers():
    customer = await CustomerService().get_customers()
    return JSONResponse(customer)


@router.post("/", response_description="Add new customer.")
async def add_new_customer(request: Request, customer: InboundCustomer = Depends(InboundCustomer.as_form)):
    # files payload sent by grpc
    protos = await form2buffer(customer.images)
    # extract proper data for customer
    data = Customer(**customer.dict(exclude={"target", "supervisor_id", "images"}))

    new_customer = await CustomerService().create_customer(jsonable_encoder(data))
    print(new_customer)
    if new_customer:
        # RequestAvatarCustomerCreation().request_avatar_customer_to_storage(new_customer, protos)
        if customer.target == TargetCustomer.Student.value and customer.supervisor_id:
            await StudentService().add_supervisor_for_student(jsonable_encoder({'student_id': new_customer, 'supervisor_id': customer.supervisor_id}))
        if customer.target == TargetCustomer.Supervisor.value:
            await SupervisorService().add_supervisor(jsonable_encoder({'supervisor_id': new_customer}))
         
        return JSONResponse(new_customer)
    
    raise HTTPException(status_code=400, detail=f"Bad request")


@router.patch("/{id}", response_description='Update a customer')
async def update_customer(id: str, req: AlterCustomerSchema = Body(...)):
    customer = await CustomerService().update_customer(id=id, customer=jsonable_encoder(req, exclude_none=True))
    if customer:
        return JSONResponse(customer)

    raise HTTPException(status_code=404, detail=f"Customer ID: {id} not found!")


@router.delete("/{id}", response_description="Delete a customer")
async def delete_customer(id: str):
    flag = await CustomerService().delete_customer(id)
    if flag:
        return JSONResponse(content=f"Record ID: {id} deteled successfully!")

    raise HTTPException(status_code=404, detail=f"Record ID: {id} not found")
