import asyncio
import logging
from concurrent import futures
from protos.generated import greeter_pb2_grpc
from services.customer import GreeterGRPCServer
import grpc


async def serve() -> None:
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    greeter_pb2_grpc.add_GreeterServicer_to_server(GreeterGRPCServer(), server)
    listen_addr = '[::]:9000'
    server.add_insecure_port(listen_addr)
    logging.info("Starting server on %s", listen_addr)
    await server.start()
    await server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    asyncio.run(serve())
