import pika
import json
from core.config import settings

class MetaClass(type):
    _instance = {}

    def __call__(cls, *args: any, **kwds: any) -> any:
        """ Singleton """
        if cls not in cls._instance:
            cls._instance[cls] = super(MetaClass, cls).__call__(*args, **kwds)
            return cls._instance[cls]

class RabbitMQ(metaclass=MetaClass):

    def __init__(self):
        self._url = pika.URLParameters(settings.CELERY_BROKER_URL)
        self._connection = pika.BlockingConnection(self._url)


class Publisher():

    def __init__(self, server, exchange='', exchange_type='', routing_key=''):
        self._server = server
        self._channel = self._server._connection.channel()
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.routing_key = routing_key
        self._channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type)

    def publish(self, payload: dict = {}):
        self._channel.basic_publish(exchange=self.exchange, routing_key=self.routing_key, body=json.dumps(payload))
        print('Published!')
        self._connection.close()


class Subscriber():
    def __init__(self, server, queue='', exchange='', routing_key=''):
        self._server = server
        self.exchange = exchange
        self.routing_key = routing_key
        self._channel = self._server._connection.channel()
        self.queue = self._channel.queue_declare(queue=queue)
        self._channel.queue_bind(exchange=self.exchange, queue=self.queue.method.queue, routing_key=self.routing_key)

    def callback(self, method, properties, body):
        # udc()
        payload = json.loads(body)
        print(' [x] Notifying {}'.format(payload['user_email']))
        print(' [x] Done')
        self.basic_ack(delivery_tag=method.delivery_tag)

    def subscribe(self):
        self._channel.basic_consume(queue=self.queue, on_message_callback=self.callback)
        print(' [*] Waiting for notify messages. To exit press CTRL+C')
        self._channel.start_consuming()