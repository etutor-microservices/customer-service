

from typing import Optional
from db.mongo_client import get_db
from models.supervisors import Supervisor, AlterSupervisorSchema


_supervisor = get_db("supervisor")
class SupervisorService():
    """
        Contains all logic for DML Operations
            """
    __instance = None

    @staticmethod
    async def add_supervisor(supervisor: Supervisor) -> Optional[Supervisor]:
        if not await _supervisor.find_one({"supervisor_id": supervisor['supervisor_id']}):
            supervisor = await _supervisor.insert_one(supervisor)
            return supervisor
        return None
    
    @staticmethod
    async def get_supervisor_by_id(supervisor_id: str)-> Optional[Supervisor]:      
        supervisor = await _supervisor.find_one({"supervisor_id": supervisor_id})
        if supervisor:
            return supervisor
        return None

    @staticmethod
    async def update_supervisor(id: str, supervisor: AlterSupervisorSchema) -> Optional[Supervisor]:
        if len(supervisor) >= 1:
            update_result = await _supervisor.update_one({"_id": id}, {"$set": supervisor})
            if update_result.modified_count == 1:
                updated_record = await _supervisor.find_one({"_id": id})
                if updated_record is not None:
                    return updated_record
        return None

    @staticmethod
    async def delete_supervisor(id: str) -> bool:
        deleted_record = await _supervisor.delete_one({"_id": id})
        if deleted_record.deleted_count == 1:
            return True
        return False