

from typing import Optional
from db.mongo_client import get_db
from models.students import AlterStudentSchema, Student


_student = get_db("student")
class StudentService():
    """
        Contains all logic for DML Operations
            """
    __instance = None

    @staticmethod
    async def add_supervisor_for_student(student: Student) -> Optional[Student]:
        if not await _student.find_one({"student_id": student['student_id']}):
            student = await _student.insert_one(student)
            return student
        return None
    
    @staticmethod
    async def get_student_by_id(student_id: str)-> Optional[Student]:      
        student = await _student.find_one({"student_id": student_id})
        if student:
            return student
        return None

    @staticmethod
    async def update_student(id: str, student: AlterStudentSchema) -> Optional[Student]:
        if len(student) >= 1:
            update_result = await _student.update_one({"_id": id}, {"$set": student})
            if update_result.modified_count == 1:
                updated_record = await _student.find_one({"_id": id})
                if updated_record is not None:
                    return updated_record
        return None

    @staticmethod
    async def delete_student(id: str) -> bool:
        deleted_record = await _student.delete_one({"_id": id})
        if deleted_record.deleted_count == 1:
            return True
        return False