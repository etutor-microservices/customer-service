from typing import List, Optional
from fastapi import File
from models.customers import ProtoUploadFile
from helpers.producer import Producer, RabbitMQ
from core.config import settings
from db.mongo_client import get_db
from models.customers import Customer, AlterCustomerSchema
from protos.generated import files_pb2
from protos.generated import files_pb2_grpc
import grpc

_customer = get_db("customer")

class RequestAvatarCustomerCreation():
    """
        gRPC methods communicating with Storage Service
            """
    __instance = None
    def __init__(self) -> None:
        pass

    @staticmethod
    def request_avatar_customer_to_storage(ownerid: str, files) -> Optional[str]:
        with grpc.insecure_channel(settings.GRPC_CONNECTION) as channel: 
            stub = files_pb2_grpc.FilesServiceStub(channel) # get stub via channel created
            response = stub.UploadForOwner(files_pb2.UploadFilesForOwnerRequest(
                ownerId=ownerid, files=files
            )) # call defined function in .proto file from client to send over server.
            return response.urls # boolean from server response.


class CustomerService():
    """
        Contains all logic for DML Operations
            """
    __instance = None

    @staticmethod
    async def get_customer_by_id(id: str)-> Optional[Customer]:      
        customer = await _customer.find_one({"_id": id})
        if customer:
            server = RabbitMQ(queue='abc_queue', routingKey='rt_secret', exchange='abc_exchange')
            rmq = Producer(server)
            rmq.publish(customer)
            return customer
        return None
    
    @staticmethod
    async def get_customers()-> Optional[List[Customer]]:   
        customers = []
        for doc in  await _customer.find().to_list(length=100):
            customers.append(doc)
        return customers

    @staticmethod
    async def create_customer(customer: Customer) -> Optional[Customer]:   
        new_customer = await _customer.insert_one(customer)
        created_record = await _customer.find_one({"_id": new_customer.inserted_id})
        if created_record:
            return created_record['_id']
        return None

    @staticmethod
    async def update_customer(id: str, customer: AlterCustomerSchema) -> Optional[Customer]:
        if len(customer) >= 1:
            update_result = await _customer.update_one({"_id": id}, {"$set": customer})
            if update_result.modified_count == 1:
                updated_record = await _customer.find_one({"_id": id})
                if updated_record is not None:
                    return updated_record
        return None
    
    @staticmethod
    async def delete_customer(id: str) -> bool:
        deleted_record = await _customer.delete_one({"_id": id})
        if deleted_record.deleted_count == 1:
            return True
        return False

