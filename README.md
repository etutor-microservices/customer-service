# eTutor Customer Service

School project that I am trying to build based on Python FastAPI, RabbitMQ, gRPC.

> This project is a part of the eTutor website.

## Implementation

- FastAPI
- MongoDB
- RabbitMQ
- gRPC

## Preview.

We are currently developing. Stay tuned.
