namespace CustomerService.Domain.Enums
{
    public enum CustomerType
    {
        Student,
        Supervisor
    }
}