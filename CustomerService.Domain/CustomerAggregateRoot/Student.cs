using System;
using CustomerService.Domain.Enums;

namespace CustomerService.Domain.CustomerAggregateRoot
{
    public class Student : Customer
    {
        public Guid? SupervisorId { get; private set; }

        private Student()
        {
        }

        public Student(string firstName, string middleName, string lastName, string email, DateTime dateOfBirth,
            Gender gender, string phoneNumber, Guid? supervisorId) : base(firstName, middleName, lastName, email,
            dateOfBirth, gender, phoneNumber)
        {
            SupervisorId = supervisorId;
        }
    }
}