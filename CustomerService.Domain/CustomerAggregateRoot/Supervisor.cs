using System;
using CustomerService.Domain.Enums;

namespace CustomerService.Domain.CustomerAggregateRoot
{
    public class Supervisor : Customer
    {
        private Supervisor()
        {
        }

        public Supervisor(string firstName, string middleName, string lastName, string email, DateTime dateOfBirth,
            Gender gender, string phoneNumber) : base(firstName, middleName, lastName, email, dateOfBirth, gender,
            phoneNumber)
        {
        }
    }
}