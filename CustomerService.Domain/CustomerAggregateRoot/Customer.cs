﻿using System;
using CustomerService.Domain.Common;
using CustomerService.Domain.Enums;

namespace CustomerService.Domain.CustomerAggregateRoot
{
    public class Customer : AuditableEntity, IAggregateRoot
    {
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public string PhoneNumber { get; private set; }
        public Gender Gender { get; private set; }

        protected Customer()
        {
        }

        public Customer(string firstName, string middleName, string lastName, string email, DateTime dateOfBirth,
            Gender gender, string phoneNumber)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Email = email;
            DateOfBirth = dateOfBirth;
            Gender = gender;
            PhoneNumber = phoneNumber;
        }

        public void UpdateFirstName(string firstName)
        {
            if (string.IsNullOrEmpty(firstName)) return;
            FirstName = firstName;
        }
    }
}