﻿using CustomerService.Application.Contracts;
using CustomerService.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CustomerService.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static void RegisterPersistenceService(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(cfg => cfg.UseSqlServer(connectionString));

            services.AddScoped(typeof(IAsyncReadRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped<ICustomersRepository, CustomersRepository>();
        }
    }
}