using CustomerService.Domain.CustomerAggregateRoot;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerService.Persistence.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.FirstName).HasMaxLength(50);
            builder.Property(c => c.MiddleName).HasMaxLength(50);
            builder.Property(c => c.LastName).HasMaxLength(50);
            builder.Property(c => c.PhoneNumber).HasMaxLength(20);

            builder.HasIndex(c => c.PhoneNumber).IsUnique();
            builder.HasIndex(c => c.Email).IsUnique();
        }
    }
}