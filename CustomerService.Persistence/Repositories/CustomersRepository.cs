using System.Linq;
using System.Threading.Tasks;
using CustomerService.Application.Contracts;
using CustomerService.Domain.CustomerAggregateRoot;
using Microsoft.EntityFrameworkCore;

namespace CustomerService.Persistence.Repositories
{
    public class CustomersRepository : BaseRepository<Customer>, ICustomersRepository
    {
        public CustomersRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Customer> GetByEmail(string email)
        {
            return await DbContext.Set<Customer>().Where(c => c.Email == email).FirstOrDefaultAsync();
        }

        public async Task<Customer> GetByPhoneNumber(string phoneNumber)
        {
            return await DbContext.Set<Customer>().Where(c => c.PhoneNumber == phoneNumber).FirstOrDefaultAsync();
        }
    }
}