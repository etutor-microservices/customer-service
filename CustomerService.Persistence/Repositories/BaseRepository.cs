using System;
using System.Threading.Tasks;
using CustomerService.Application.Contracts;
using CustomerService.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace CustomerService.Persistence.Repositories
{
    public class BaseRepository<T> : IAsyncReadRepository<T>, IAsyncRepository<T> where T : class, IAggregateRoot
    {
        protected ApplicationDbContext DbContext { get; }

        public BaseRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await DbContext.Set<T>().FindAsync(id);
        }

        public async Task<T> AddAsync(T entity)
        {
            var inserted = DbContext.Add(entity).Entity;
            await DbContext.SaveChangesAsync();
            return inserted;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            await DbContext.SaveChangesAsync();
            return entity;
        }
    }
}