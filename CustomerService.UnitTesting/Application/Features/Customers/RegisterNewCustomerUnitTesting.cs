using System;
using System.Threading.Tasks;
using AutoMapper;
using CustomerService.Application.Contracts;
using CustomerService.Application.Features.Customers.Commands.RegisterNewCustomer;
using CustomerService.Application.MappingProfile;
using CustomerService.Domain.CustomerAggregateRoot;
using CustomerService.Domain.Enums;
using FluentAssertions;
using Moq;
using Xunit;

namespace CustomerService.UnitTesting.Application.Features.Customers
{
    public class RegisterNewCustomerUnitTesting
    {
        private readonly Mock<ICustomersRepository> _customerMockRepository;
        private readonly IMapper _mapper;

        private readonly Customer _customer = new("First", "Middle", "Last", "example@gmail.com",
            DateTime.Parse("1990-09-09"), Gender.Male, "08834578989");


        public RegisterNewCustomerUnitTesting()
        {
            _customerMockRepository = new Mock<ICustomersRepository>();
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()).CreateMapper();

            _customerMockRepository.Setup(x => x.AddAsync(It.IsAny<Customer>())).ReturnsAsync(_customer);
        }

        [Fact]
        public async Task RegisterNewCustomer_ShouldReturnExpectedCustomer()
        {
            var validator = new RegisterNewCustomerValidator(_customerMockRepository.Object);

            var handler =
                new RegisterNewCustomerHandler(_customerMockRepository.Object, validator, _mapper);

            var request = new RegisterNewCustomer
            {
                FirstName = "First",
                MiddleName = "Middle",
                LastName = "Last",
                PhoneNumber = "08834578989",
                DateOfBirth = DateTime.Parse("1990-09-09"),
                CustomerType = "Student",
                Gender = "Male",
                Email = "example@gmail.com"
            };
            var (errors, customer) = await handler.Handle(request, default);

            errors.Should().BeNull();
            customer.FirstName.Should().Be(_customer.FirstName);
            _customerMockRepository.Verify(x => x.AddAsync(It.IsAny<Customer>()));
        }

        [Fact]
        public async Task RegisterNewCustomer_InvalidRequest_ShouldReturnExpectedErrors()
        {
            var validator = new RegisterNewCustomerValidator(_customerMockRepository.Object);

            var handler =
                new RegisterNewCustomerHandler(_customerMockRepository.Object, validator, _mapper);

            var request = new RegisterNewCustomer
            {
                FirstName = "First",
                MiddleName = "Middle",
                LastName = "Last",
                PhoneNumber = "08834578989",
                DateOfBirth = DateTime.Parse("1990-09-09"),
                CustomerType = "worker",
                Gender = "undefined",
                Email = "example@gmail.com"
            };
            var (errors, customer) = await handler.Handle(request, default);

            errors.Should().NotBeEmpty();
            customer.Should().BeNull();
            _customerMockRepository.Verify(x => x.AddAsync(It.IsAny<Customer>()), Times.Never);
        }
    }
}