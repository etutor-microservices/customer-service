using System;
using System.Threading.Tasks;
using AutoMapper;
using CustomerService.Application.Contracts;
using CustomerService.Application.Features.Customers.Commands.UpdateCustomerInfo;
using CustomerService.Application.MappingProfile;
using CustomerService.Domain.CustomerAggregateRoot;
using CustomerService.Domain.Enums;
using FluentAssertions;
using Moq;
using Xunit;

namespace CustomerService.UnitTesting.Application.Features.Customers
{
    public class UpdateCustomerInfoUnitTesting
    {
        private readonly Mock<ICustomersRepository> _customerMockRepository;
        private readonly IMapper _mapper;

        private readonly Customer _customer = new("First", "Middle", "Last", "example@gmail.com",
            DateTime.Parse("1990-09-09"), Gender.Male, "08834578989");

        public UpdateCustomerInfoUnitTesting()
        {
            _customerMockRepository = new Mock<ICustomersRepository>();
            _mapper = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); }).CreateMapper();

            _customerMockRepository.Setup(x => x.UpdateAsync(It.IsAny<Customer>())).ReturnsAsync(_customer);
        }

        [Fact]
        public async Task ValidRequest_ShouldReturnExpectedCustomer()
        {
            var validator = new UpdateCustomerInfoValidator();
            var request = new UpdateCustomerInfo
            {
                FirstName = "Updated"
            };

            var handler = new UpdateCustomerInfoHandler(_customerMockRepository.Object, validator, _mapper);
            var (errors, customer) = await handler.Handle(request, default);

            errors.Should().BeNull();
            _customerMockRepository.Setup(x => x.UpdateAsync(It.IsAny<Customer>()));
        }
    }
}