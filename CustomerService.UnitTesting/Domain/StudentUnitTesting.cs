using System;
using CustomerService.Domain.CustomerAggregateRoot;
using CustomerService.Domain.Enums;
using FluentAssertions;
using Xunit;

namespace CustomerService.UnitTesting.Domain
{
    public class StudentUnitTesting
    {
        [Fact]
        public void Creation_ShouldReturnExpectedStudent()
        {
            var firstName = "First";
            var middleName = "Middle";
            var lastName = "LastName";
            var email = "example@gmail.com";
            var dateOfBirth = DateTime.Parse("2000-06-21");
            var gender = Gender.Female;
            var phone = "0937647862";
            var supervisorId = Guid.NewGuid();

            var student = new Student(firstName, middleName, lastName, email, dateOfBirth, gender, phone, supervisorId);

            student.FirstName.Should().Be(firstName);
            student.MiddleName.Should().Be(middleName);
            student.LastName.Should().Be(lastName);
            student.DateOfBirth.Should().Be(dateOfBirth);
            student.Gender.Should().Be(gender);
            student.PhoneNumber.Should().Be(phone);
            student.SupervisorId.Should().Be(supervisorId);
        }
    }
}