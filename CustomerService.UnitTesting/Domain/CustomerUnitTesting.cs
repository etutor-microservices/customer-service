using System;
using CustomerService.Domain.CustomerAggregateRoot;
using CustomerService.Domain.Enums;
using FluentAssertions;
using Xunit;

namespace CustomerService.UnitTesting.Domain
{
    public class CustomerUnitTesting
    {
        [Fact]
        public void Creation_ShouldReturnExpectedCustomer()
        {
            var firstName = "First";
            var middleName = "Middle";
            var lastName = "LastName";
            var email = "example@gmail.com";
            var dateOfBirth = DateTime.Parse("2000-06-21");
            var gender = Gender.Male;
            var phone = "0937647862";

            var customer = new Customer(firstName, middleName, lastName, email, dateOfBirth, gender, phone);

            customer.FirstName.Should().Be(firstName);
            customer.MiddleName.Should().Be(middleName);
            customer.LastName.Should().Be(lastName);
            customer.DateOfBirth.Should().Be(dateOfBirth);
            customer.Gender.Should().Be(gender);
            customer.PhoneNumber.Should().Be(phone);
        }
    }
}